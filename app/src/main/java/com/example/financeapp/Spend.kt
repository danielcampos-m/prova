package com.example.financeapp

import android.os.Parcel
import android.os.Parcelable

class Spend(
        var categoria:String?,
        var descricao:String?,
        var valor:Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
            categoria = parcel.readString(),
            descricao = parcel.readString(),
            valor = parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel?.writeString(categoria)
        parcel?.writeString(descricao)
        parcel.writeInt(valor)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "Spend(categoria=$categoria, descricao=$descricao, valor=$valor)"
    }

    companion object CREATOR : Parcelable.Creator<Spend> {
        override fun createFromParcel(parcel: Parcel): Spend {
            return Spend(parcel)
        }

        override fun newArray(size: Int): Array<Spend?> {
            return arrayOfNulls(size)
        }
    }


}