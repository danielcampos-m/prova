package com.example.financeapp

import android.app.Activity
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.*

class SpendListBuilder(private val activity: Activity, private val rvSpending: RecyclerView, private val spending: ArrayList<Spend>) : SpendAdapter.OnClickSpendListener {
    private var spendAdapter: SpendAdapter? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    fun load() {
        Log.d(MainActivity.TAG, "Load!")
        spendAdapter = SpendAdapter(spending)
        spendAdapter?.setOnClickSpendListener(this)
        linearLayoutManager = LinearLayoutManager(activity)
        rvSpending.layoutManager = linearLayoutManager
        rvSpending.setHasFixedSize(true)
        //DividerItemDecoration itemDecoration = new DividerItemDecoration(this.activity, this.linearLayoutManager.getOrientation());
        //this.rvSpending.addItemDecoration(itemDecoration);
        rvSpending.adapter = spendAdapter
    }

    override fun onClickSpend(spend: Spend?) {}
}