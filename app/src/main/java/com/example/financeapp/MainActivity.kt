package com.example.financeapp

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.RecyclerView
import java.io.*
import java.lang.Exception
import java.lang.StringBuilder
import java.net.URI
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class MainActivity : AppCompatActivity() {

    private val REQ_NEWSPEND_CODE = 1024
    private var txtBalance: TextView? = null
    lateinit var lvSpends: RecyclerView
    lateinit var spendListBuilder:SpendListBuilder
    var initialized: Boolean = false
    var FILENAME:String = "finance_report.csv"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        println("Voltou ao OnCreate")
        setContentView(R.layout.activity_main)
        txtBalance = findViewById(R.id.txtSaldo)
    }

    fun onClickNewSpend(view: View){
        println("Entrou click new spend")
        var newSpendIntent =  Intent(this, NewSpendActivity::class.java)
        println("chamando startactiforresult")
        startActivityForResult(newSpendIntent, REQ_NEWSPEND_CODE)
        println("Voltou ao metodo onclicknewspend")
    }

    override fun onResume() {
        super.onResume()
        println("ENTORU NO RESUME!!")
        txtBalance?.text = SpendDAOSingleton.getBalance()
        lvSpends = findViewById(R.id.rvSpending)
        this.spendListBuilder = SpendListBuilder(
                this,
                this.lvSpends,
                SpendDAOSingleton.spends
        )
        this.spendListBuilder.load()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        println("ENTROU  EM ACTIVITYYRESULT")

        if( (requestCode == REQ_NEWSPEND_CODE) && (resultCode == RESULT_OK) && (data != null) ) {
            var spend: Spend? = data.getParcelableExtra<Spend>(NewSpendActivity.NEWSPEND_KEY)
            SpendDAOSingleton.addStore(spend as Spend)
        }
    }

    //ARQUIVOS SALVOS EM COM.EXAMPLE.FINANCEAPP.FILES
    fun onReport(view:View){
    //param context:Context,
        var data = StringBuilder()
        SpendDAOSingleton.spends.forEach {
            data.append(it.toString())
        }
        println("DATA: ${data.toString()}")

        var filename = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"))
        println("NEW FILENAME: ${filename}")
        var file:File =File(applicationContext.filesDir,filename+".csv")
        try {
            var bfWriter:BufferedWriter = BufferedWriter(FileWriter(file))
            bfWriter.write(data.toString())
            bfWriter.close()
            Toast.makeText(applicationContext,"File successfully saved ",Toast.LENGTH_SHORT).show()
        }catch (e:IOException){
            Toast.makeText(applicationContext,"File error: "+e.message,Toast.LENGTH_SHORT).show()
            Log.e("FinanceApp error", "mensagem man")
            e.printStackTrace()
        }

//        var data = StringBuilder()
//        SpendDAOSingleton.spends.forEach {
//            data.append(it.toString())
//        }
//        println("Ok data preparada")
//        try{
//            var out:FileOutputStream = openFileOutput("data.csv", Context.MODE_PRIVATE)
//            out.write(data.toString().toByteArray())
//            out.close()
//            println("OK OUT WRITED ${out}")
//
//            var c:Context = applicationContext
//            var fileloc:File = File(filesDir,"data.csv")
//            var path: Uri? = FileProvider.getUriForFile(c,"com.example.financeapp",fileloc)
//            var fileIntent:Intent = Intent(Intent.ACTION_SEND)
//            fileIntent.putExtra(Intent.EXTRA_SUBJECT,"Data")
//            fileIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
//            fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
//            fileIntent.putExtra(Intent.EXTRA_STREAM,path)
//            startActivity(Intent.createChooser(fileIntent,"Send mail"))
//        }catch(e:Exception){
//            e.printStackTrace()
//        }

    }
    companion object {
        const val TAG = "Debugand"
    }
}