package com.example.financeapp

import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class SpendViewHolder(itemView: View, adapter: SpendAdapter) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
    private val txtDescription: TextView
    private val txtValue: TextView
    private val txtCategory: TextView
    private val llSpend: LinearLayout
    private val adapter: SpendAdapter
    private var spendItemList: ItemList<Spend>? = null

    fun bind(spendItemList: ItemList<Spend>) {
        txtCategory.setText(spendItemList.item.categoria)
        txtDescription.setText(spendItemList.item.descricao)
        txtValue.text = "$" + spendItemList.item.valor
        val visibility = if (spendItemList.expanded) View.VISIBLE else View.GONE
        txtDescription.visibility = visibility
        this.spendItemList = spendItemList
        if (spendItemList.item.categoria.equals("Crédito")) {
            Log.d(MainActivity.TAG, "É Crédito")
            llSpend.setBackgroundColor(itemView.resources.getColor(R.color.colorAccent, null))
        } else {
            llSpend.setBackgroundColor(itemView.resources.getColor(R.color.colorDanger, null))
        }
        txtCategory.setText(spendItemList.item.descricao)
    }

    private fun toggleDescription() {
        val visibility = if (txtDescription.visibility == View.GONE) View.VISIBLE else View.GONE
        txtDescription.visibility = visibility
        spendItemList!!.expanded = visibility == View.VISIBLE
    }

    override fun onClick(v: View) {
        if (v.id == itemView.id) toggleDescription()
    }

    init {
        Log.d(MainActivity.TAG, "constructor")
        txtCategory = itemView.findViewById(R.id.txtDesc)
        txtDescription = itemView.findViewById(R.id.txtDescription)
        txtValue = itemView.findViewById(R.id.txtValor)
        llSpend = itemView.findViewById(R.id.llvSpend)
        itemView.setOnClickListener(this)
        this.adapter = adapter
    }
}