package com.example.financeapp

object SpendDAOSingleton{

    var spends: ArrayList<Spend>
    init {
        SpendDAOSingleton.spends = ArrayList<Spend>()
    }
    fun addStore(spend:Spend){
        this.spends.add(spend)
    }

    fun getBalance():String{
        println("Entrou no getBalance")
        var valor:Float = 0.toFloat()
//        println("No array ha ${}")
        spends.forEach{
            if(!it.categoria.equals("Crédito"))
                valor -= it.valor
            else
                valor += it.valor
        }
        return "$"+valor;
    }
}
