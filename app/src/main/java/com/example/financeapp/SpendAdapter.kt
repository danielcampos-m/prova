package com.example.financeapp

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import java.util.*

class SpendAdapter(spending: ArrayList<Spend>) : RecyclerView.Adapter<SpendViewHolder>() {
    private val spending: ArrayList<ItemList<Spend>>
    lateinit var listener: OnClickSpendListener

    interface OnClickSpendListener {
        fun onClickSpend(spend: Spend?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpendViewHolder {
        Log.d(MainActivity.TAG, "Inflando")
        val inflater = LayoutInflater.from(parent.context)
        val itemListView = inflater.inflate(R.layout.itemlist_view_spend, parent, false)
        return SpendViewHolder(itemListView, this)
    }

    override fun onBindViewHolder(holder: SpendViewHolder, position: Int) {
        Log.d(MainActivity.TAG, "onBind")
        holder.bind(spending[position])
    }

    override fun getItemCount(): Int {
        Log.d(MainActivity.TAG, "getItemCount" + spending.size)
        return spending.size
    }

    fun setOnClickSpendListener(listener: OnClickSpendListener) {
        this.listener = listener
    }

    fun getOnClickSpendListener(): OnClickSpendListener? {
        return this.listener
    }

    init {
        Log.d(MainActivity.TAG, "Spend Adapter")
        this.spending = ArrayList<ItemList<Spend>>()

        for (spend in spending) {
            val itemList:ItemList<Spend> = ItemList(spend)
            this.spending.add(itemList)
        }
    }
}