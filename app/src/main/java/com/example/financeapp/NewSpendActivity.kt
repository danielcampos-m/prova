package com.example.financeapp

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class NewSpendActivity : AppCompatActivity() {

    var spnCat: Spinner? = null
    var txtDesc: EditText? = null
    var txtValue: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_spend)
        spnCat = findViewById(R.id.spnCategory)
        txtDesc = findViewById(R.id.edTxtDesc)
        txtValue = findViewById(R.id.edTxtValue)
    }

    fun onClickSave(view: View){
        if(txtDesc?.text?.isEmpty() == true || txtValue?.text?.isEmpty() == true){
            Toast.makeText(this, "All fields must be filled", Toast.LENGTH_LONG).show()
            return
        }
        println("Saiu do condicional")
        var spend:Spend = Spend(spnCat?.selectedItem.toString(), txtDesc?.text.toString(),txtValue?.text.toString().toInt())
        println("criou spend ${spend}")
        var output:Intent = Intent()
        println("criou intent")
        output.putExtra(NEWSPEND_KEY,spend)
        println("chamou putextra do output")
        setResult(RESULT_OK, output)
        println("setresult feito")
        finish()
        println("Saindo do metodo")
    }

    companion object {
        const val NEWSPEND_KEY = "NewSpendActivity.NEWSPEND_KEY"
    }

}